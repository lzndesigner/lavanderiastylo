<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\ContactMail;
use Illuminate\Support\Facades\Mail;
use DB;
use Carbon\Carbon;

class HomeController extends Controller
{
  // Dashboard
  public function index()
  {
    return view('frontend.base');
  }
  
  public function sendmail(Request $request)
  {
    $remove = ['(', ')', ' ', '-'];
    $insert = ['', '', '', ''];
    $phoneMask = str_replace($remove, $insert, $request->phone);

    $request->request->add(['phoneMask' => $phoneMask]);

    Mail::send(new ContactMail($request));
    return redirect('/');
  }
  
}
