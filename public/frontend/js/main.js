$(function() {
    "use strict"; /* Global variables */
    var $win = $(window); /*==========Mobile Menu==========*/
    var $navToggler = $('.navbar-toggler');
    $navToggler.on('click', function() {
        $(this).toggleClass('actived');
    });
    $navToggler.on('click', function() {
        $('.navbar-collapse').toggleClass('menu-opened');
    }); /*==========Sticky Navbar==========*/
    $win.on('scroll', function() {
        if ($win.width() >= 992) {
            var $navbar = $('.navbar');
            if ($win.scrollTop() > 50) {
                $navbar.addClass('is-sticky');
            } else {
                $navbar.removeClass('is-sticky');
            }
        }
    }); /*==========Set Background-img to section==========*/
    $('.bg-img').each(function() {
        var imgSrc = $(this).children('img').attr('src');
        $(this).parent().css({
            'background-image': 'url(' + imgSrc + ')',
            'background-size': 'cover',
            'background-position': 'center',
        });
        $(this).parent().addClass('bg-img');
        if ($(this).hasClass('background-size-auto')) {
            $(this).parent().addClass('background-size-auto');
        }
        $(this).remove();
    }); /*==========Slick Carousel==========*/
    $('.slick-carousel').slick(); /*==========NiceSelect Plugin==========*/
    $('select').niceSelect();
});